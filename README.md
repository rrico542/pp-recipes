# pp-recipes

## Prerequisites : (as root)
```
apt-get install debos bmap-tools
```

## Build :
```
debos --scratchsize=8G --memory=3GB --cpus=4 -t environment:openbox rootfs.yaml
```

Of course you can build another environment

## Bmap :
```
bmaptool create pinephone-openbox.img > pinephone-openbox.img.bmap
```

Change image name according to your environment choice

## Burn : (as root)
```
bmaptool copy pinephone-openbox.img /dev/mmcblk0
```

## Progress :

| Environment     | Build               | Start               | Optimized      | Why ?          |
| --------------- |:-------------------:|:-------------------:|:--------------:|:--------------:|
| openbox         | :large_blue_circle: | :large_blue_circle: | :white_circle: | I love it !    |
| blackbox        | :white_circle:      | :red_circle:        | :red_circle:   |                |
| jwm             | :white_circle:      | :large_blue_circle: | :red_circle:   |                |
| lxde            | :large_blue_circle: | :large_blue_circle: | :white_circle: |                |
| xfce4           | :white_circle:      | :large_blue_circle: | :red_circle:   |                |
| enlightenment   | :large_blue_circle: | :large_blue_circle: | :white_circle: |                |
| budgie          | :black_circle:      | :black_circle:      | :black_circle: | Too Large      |
| cinnamon        | :black_circle:      | :black_circle:      | :black_circle: | Too Large      |
| gnome           | :large_blue_circle: | :large_blue_circle: | :white_circle: |                |
| gnome-flashback | :black_circle:      | :black_circle:      | :black_circle: | Too Large      |
| lxlauncher      | :black_circle:      | :black_circle:      | :black_circle: | Too Large      |
| lxqt            | :black_circle:      | :black_circle:      | :black_circle: | Too Large      |
| matchbox        | :black_circle:      | :black_circle:      | :black_circle: | Too old :)     |
| mate            | :black_circle:      | :black_circle:      | :black_circle: | Too Large      |
| plasma          | :black_circle:      | :black_circle:      | :black_circle: | Too Large      |
| ukui            | :black_circle:      | :black_circle:      | :black_circle: | Too Large      |


- :black_circle: = Will not be part of that work
- :red_circle: = No
- :white_circle: = In progress
- :large_blue_circle: = Yes


Need some never tested WM ? please ask.
